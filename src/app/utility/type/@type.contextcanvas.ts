import { fabric } from "fabric";
import { Dispatch,SetStateAction } from 'react'

export type TypeFabricCanvas = {
    canvas?: fabric.Canvas
    setCanvas?: Dispatch<SetStateAction<any>>,
    isDrawing?: boolean,
    setIsDrawing?: Dispatch<SetStateAction<boolean>>
}

interface InterfaceContextCanvas extends TypeFabricCanvas  {}


export default  InterfaceContextCanvas;