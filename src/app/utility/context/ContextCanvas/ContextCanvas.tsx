'use client'
import React from 'react';
import InterfaceContextCanvas from '../../type/@type.contextcanvas';
import {fabric} from 'fabric';

export const ContextCanvas = React.createContext<InterfaceContextCanvas>({});

const ContextCanvasProvider: React.FC<{children: React.ReactNode}> = ({ children }) => {
    const [canvas, setCanvas] = React.useState<fabric.Canvas>()
    const [isDrawing,setIsDrawing] = React.useState(false) 
    React.useEffect(() => {
      const c = new fabric.Canvas("canvas", {
        height: 1500,
        width: 1500,
        backgroundColor: "#eee",
        isDrawingMode: isDrawing 
      });
  
      // settings for all canvas in the app
      fabric.Object.prototype.transparentCorners = false;
      fabric.Object.prototype.cornerColor = "red";
      fabric.Object.prototype.cornerStyle = "rect";
      fabric.Object.prototype.cornerStrokeColor = "red";
      fabric.Object.prototype.cornerSize = 6;
      setCanvas?.(c);
  
      return () => {
       c.dispose();
      };
    }, [isDrawing]);


    // Zoom in/out mouse
    canvas?.on('mouse:wheel', function(opt) {
      var delta = opt.e.deltaY;
      var zoom = canvas.getZoom();
      zoom *= 0.999 ** delta;
      if (zoom > 20) zoom = 20;
      if (zoom < 0.01) zoom = 0.01;
      canvas.setZoom(zoom);
      opt.e.preventDefault();
      opt.e.stopPropagation();
    })
    
    
    
    return <ContextCanvas.Provider value={{ canvas, setCanvas,isDrawing,setIsDrawing }}>
        {children} 
    </ContextCanvas.Provider>
}


export default ContextCanvasProvider;