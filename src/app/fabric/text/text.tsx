import { fabric } from "fabric";


export default function Text(canvas?: fabric.Canvas) {
// Define an array with all fonts

var textbox = new fabric.Textbox('Lorum ipsum dolor sit amet', {
  left: 50,
  top: 50,
  width: 150,
  fontSize: 20
});

// @ts-ignore
canvas?.add(textbox).setActiveObject(textbox);
}
