import { fabric } from "fabric";


const addRect = (canvas?: fabric.Canvas) => {
    const rect = new fabric.Rect({
      height: 100,
      width: 100,
      stroke: "#2BEBC8",
      fill: "#222"
    });
    canvas?.add(rect);
    canvas?.requestRenderAll();
  };

export default addRect