"use client";
import { ContextCanvas } from "@/app/utility/context/ContextCanvas/ContextCanvas";
import { useContext, useEffect } from "react";

function useRemoveShape() {
  const { canvas } = useContext(ContextCanvas);

  useEffect(() => {
    document.addEventListener("keydown", (e) => {      
      console.log(canvas?.getActiveObjects())
      if (e.key === "Delete") {
        // @ts-ignore
        canvas?.getActiveObjects().forEach((obj) => {canvas.remove(obj);});
      }
    });
  }, [canvas]);

}

export default useRemoveShape;
