'use client'
import React from "react";
import useRemoveShape from "./fabric/removeShape/useRemoveShape";


export default function Home() {
    useRemoveShape()

    
  return (
    <>
    <div className="flex gap-2">
    </div>
    <canvas id="canvas" />
    </>
  );
}
