'use client'
import addCirlce from "@/app/fabric/bulat/bulat";
import addRect from "@/app/fabric/kotak/kotak";
import Text from "@/app/fabric/text/text";
import { ContextCanvas } from "@/app/utility/context/ContextCanvas/ContextCanvas";
import { useContext } from 'react'

export default function Navbar() {
  const { canvas, setIsDrawing, isDrawing } = useContext(ContextCanvas)

  const toggleButtonTools = (typeButton: number) => {

    switch (typeButton) {
      case 1:
        setIsDrawing?.(true && !isDrawing)
        break;
      case 2:
        addRect(canvas)
        setIsDrawing?.(false)
        break;
      case 3:
        addCirlce(canvas)
        break;
      case 4:
        Text(canvas)
        break;
      default:
        throw new Error
    }
  }

  return (
    <nav className="bg-tarnsparent p-4 flex gap-5 items-start justify-center flex-col h-screen w-14 absolute z-50  ">
      <button onClick={() => toggleButtonTools(1)} className="text-xl font-bold">&#x270E;</button>
      <button onClick={() => toggleButtonTools(2)} className="text-xl font-bold">&#x25AE;</button>
      <button onClick={() => toggleButtonTools(3)} className="text-xl font-bold">&#x23FA;</button>
      <button onClick={() => toggleButtonTools(4)} className="text-xl font-bold ml-[2px]">T</button>
    </nav>
  )
}
